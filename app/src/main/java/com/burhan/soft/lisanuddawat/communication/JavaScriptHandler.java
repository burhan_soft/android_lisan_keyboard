package com.burhan.soft.lisanuddawat.communication;

import android.webkit.JavascriptInterface;

import com.burhan.soft.lisanuddawat.ui.PaymentActivity;

public class JavaScriptHandler {
    PaymentActivity parentActivity;

    public JavaScriptHandler(PaymentActivity activity) {
        parentActivity = activity;
    }

    @JavascriptInterface
    public void setResult(String val) {
        this.parentActivity.javascriptCallFinished(val);
    }
}