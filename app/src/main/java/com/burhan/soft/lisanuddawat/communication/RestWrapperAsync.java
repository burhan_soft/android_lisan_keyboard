package com.burhan.soft.lisanuddawat.communication;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.burhan.soft.lisanuddawat.util.Util;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class RestWrapperAsync extends AsyncTask<Void, Integer, Object> {

    private WeakReference<ResponseCallback> mResponseCallback;
    private WeakReference<ProgressCallback> mProgressCallback;

    public interface ResponseCallback {
        public void OnRequestSuccess(String response);

        public void OnRequestError(Exception ex);
    }

    public interface ProgressCallback {
        public void OnProgressUpdate(int progress);
    }

    public void setResponseCallback(ResponseCallback callback) {
        mResponseCallback = new WeakReference<ResponseCallback>(callback);
    }

    public void setProgressCallback(ProgressCallback callback) {
        mProgressCallback = new WeakReference<ProgressCallback>(callback);
    }

    private boolean getDetailsFromPost = false;

    public void SetGetDetailsFromPost(boolean value) {
        getDetailsFromPost = value;
    }

    HttpURLConnection mConnection;
    String mFormBody;
    File mUploadFile;
    String mUploadFileName;
    JSONArray mFormBodyJSONArray;
    JSONObject mFormBodyJSON;

    /**
     * Sets up the connection and the url
     *
     * @param siteUrl
     */
    public void SetUp(String siteUrl, boolean doOutput) {
        try {
            URL url = null;
            if (siteUrl != null) {
                url = new URL(siteUrl);
            } else {
                url = new URL("https://xxxx");
            }
            mConnection = (HttpURLConnection) url.openConnection();
            mConnection.setReadTimeout(10000 /* milliseconds */);
            mConnection.setConnectTimeout(5000 /* milliseconds */);
            mConnection.setRequestMethod("GET"); //set to get initially

            if (doOutput) {
                //mConnection.setDoOutput(true);
                mConnection.setDoInput(true);
            } else
                mConnection.setDoInput(true);
        } catch (Exception ex) {
            Log.e("ERROR", "Exception setting up connection:" + ex.getMessage());
        }
    }

    /**
     * Sets up form body for post requests
     *
     * @param formData
     */
    public void setFormBody(List<NameValuePair> formData) {
        if (formData == null) {
            mFormBody = "";
            return;
        }
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < formData.size(); i++) {
                NameValuePair item = formData.get(i);
                sb.append(URLEncoder.encode(item.getName(), "UTF-8"));
                sb.append("=");
                sb.append(URLEncoder.encode(item.getValue(), "UTF-8"));
                if (i != (formData.size() - 1)) {
                    sb.append("&");
                }
            }
            mFormBody = sb.toString();
        } catch (Exception ex) {
            Log.e("ERROR", "Exception setting up connection:" + ex.getMessage());
        }
    }

    /**
     * Sets up form body for post request
     *
     * @param object
     */
    public void setFormBodyJson(JSONArray object) {
        mFormBodyJSONArray = object;
    }

    /**
     * Sets up form body for post request
     *
     * @param object
     */
    public void setFormBodyJson(JSONObject object) {
        mFormBodyJSON = object;
    }

    /**
     * Write out Json array to outputstream
     *
     * @param charset
     * @param output
     * @throws IOException
     */
    private void writeFormDataJSON(JSONArray charset, OutputStream output) throws IOException {
        try {
            output.write(charset.toString().getBytes());
            output.flush();
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    /**
     * Write out Json object to outputstream
     *
     * @param charset
     * @param output
     * @throws IOException
     */
    private void writeFormDataJSON(JSONObject charset, OutputStream output) throws IOException {
        try {
            output.write(charset.toString().getBytes());
            output.flush();
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    /**
     * Write out Charset to outputStream
     *
     * @param charset
     * @param output
     * @throws IOException
     */
    private void writeFormData(String charset, OutputStream output) throws IOException {
        try {
            output.write(mFormBody.getBytes(charset));
            output.flush();
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    /**
     * Sets up authentication
     *
     * @param username
     * @param password
     */
    public void SetAuthentication(String username, String password) {
        if (this.mConnection == null) {
            Log.e("RestWrapper", "Autentication called, but connection = null");
        } else
            attachBasicAuthentication(mConnection, username, password);
    }

    /**
     * Attaches authorization to header
     *
     * @param connection
     * @param username
     * @param password
     */
    private static void attachBasicAuthentication(URLConnection connection, String username, String password) {
        //Add Basic Authentication Headers
        String userpassword = username + ":" + password;
        String encodedAuthorization =
                Base64.encodeToString(userpassword.getBytes(), Base64.NO_WRAP);
        connection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
    }

    public void setUploadFile(File file, String fileName) {
        mUploadFile = file;
        mUploadFileName = fileName;
    }

    /**
     * Do network connection in background
     */
    @Override
    protected Object doInBackground(Void... arg0) {
        return CallService();
    }

    /**
     * The actual call to the web api's
     *
     * @return
     */
    public Object CallService() {
        try {

            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 20 * 1000);

            HttpPost post = new HttpPost(APIConstant.SERVER_URL);
            StringEntity se = new StringEntity("json=" + mFormBodyJSON.toString());
            post.addHeader("content-type", "application/x-www-form-urlencoded");
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("json", mFormBodyJSON.toString()));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response;
            response = client.execute(post);
            int status = 0;
            status = response.getStatusLine().getStatusCode();

            if (status != HttpStatus.SC_OK) {
                return status;
            }
            String resFromServer = org.apache.http.util.EntityUtils.toString(response.getEntity());
            return resFromServer;
        } catch (Exception ex) {
            return ex;
        }
    }

    @Override
    protected void onPostExecute(Object result) {
        Util.stopProgressBar();
        if (mResponseCallback != null && mResponseCallback.get() != null) {
            if (result instanceof String) {
                mResponseCallback.get().OnRequestSuccess((String) result);
            } else if (result instanceof SocketTimeoutException || result instanceof ConnectTimeoutException) {
                result = new SocketTimeoutException("Failed to connect to the server. Please check your data network/GPRS/3G connectivity and wait till data connectivity arrive. \nIt may occurred due to poor network connectivity.");
                mResponseCallback.get().OnRequestError((Exception) result);
            } else {
                mResponseCallback.get().OnRequestError(new IOException("Internal server error occurred, please contact customer support."));
            }

            mResponseCallback = null;
            mProgressCallback = null;
        }
    }
}