package com.burhan.soft.lisanuddawat.ui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.Cache;
import com.burhan.soft.lisanuddawat.communication.business.CommunicationHandlerManager;
import com.burhan.soft.lisanuddawat.communication.data.RequestResponseVO;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;
import com.burhan.soft.lisanuddawat.util.Util;
import com.dd.processbutton.ProcessButton;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class VerificationActivity extends AbstractUIActivity implements
        MaterialEditText.OnClickListener
{

    private MaterialEditText emailText;
    private MaterialEditText firstNameText;
    private MaterialEditText lastNameText;
    private MaterialEditText phoneNumberText;
    private MaterialEditText dobText;
    private MaterialEditText itsIdText;
    private RadioButton maleRadioButton;
    private RadioButton femaleRadioButton;
    private MaterialEditText addressText;
    private DatePickerDialog datePicker;
    private ProcessButton processButton;
    private SimpleDateFormat dateFormatter;
    private Context context;
    private boolean isError;
    private String errorMessage;

    private Handler handler = Util.registerWeakHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        LisanApplication.setHandler(handler);
        context = this;
        setTitle(getString(R.string.verification));

        emailText = (MaterialEditText) findViewById(R.id.email);
        firstNameText = (MaterialEditText) findViewById(R.id.firstName);
        lastNameText = (MaterialEditText) findViewById(R.id.lastName);
        phoneNumberText = (MaterialEditText) findViewById(R.id.phoneNumber);
        dobText = (MaterialEditText) findViewById(R.id.dob);
        addressText = (MaterialEditText) findViewById(R.id.address);
        itsIdText = (MaterialEditText) findViewById(R.id.itsid);
        processButton = (ProcessButton) findViewById(R.id.btnContinue);
        maleRadioButton = (RadioButton) findViewById(R.id.male);
        femaleRadioButton = (RadioButton) findViewById(R.id.female);
        emailText.setText(Util.getEmail(this));

        //dobText.setOnClickListener(this);
        dateFormatter = new SimpleDateFormat(APIConstant.DATE_FORMAT, Locale.US);
//        final Calendar newCalendar = Calendar.getInstance();
//        datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener()
//        {
//
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
//            {
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//                if (newDate.before(newCalendar))
//                {
//                    dobText.setText(dateFormatter.format(newDate.getTime()));
//                } else
//                {
//                    Util.showMessage(context, getString(R.string.warning), getString(R.string.dob_validation_error));
//                }
//            }
//
//        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        Intent intent = getIntent();
        try
        {
            boolean isUserIsDeactivated = intent.getBooleanExtra(APIConstant.DEACTIVATE_USER_BY_OTHER, false);
            if (isUserIsDeactivated)
            {
                emailText.setText(intent.getStringExtra(APIConstant.DEACTIVATED_USER_EMAIL));
                isError = true;
                errorMessage = intent.getStringExtra(APIConstant.ERROR_MESSAGE);
            }
        } catch (Exception e)
        {
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Intent intent = getIntent();
        boolean isNotRegistered = intent.getBooleanExtra(APIConstant.IS_NOT_REGISTERED, false);
        if (isNotRegistered)
        {
            new Handler().postDelayed(runnable, 1 * 500);
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
    }

    private Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (Util.isThisKeyboardSetAsDefaultIME(getApplicationContext()))
            {
                try
                {
                    InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
                    imeManager.showInputMethodPicker();

                } catch (Throwable t)
                { // java.lang.NoSuchMethodError if API_level<11
                    t.printStackTrace();
                }
            }

            if (isError)
            {
                isError = false;
                Util.showMessage(context, context.getString(R.string.error), errorMessage);
            }
        }
    };

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_verification;
    }

    /**
     * Handle messages received from the handler
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message)
    {
        stop();
        changeState(true);
        Object o = message.obj;
        if (o instanceof Exception)
        {
            Util.showMessage(this, getString(R.string.error), ((Exception) o).getMessage());
            return;
        }
        processResponse(o);
    }

    private void processResponse(Object o)
    {
        if (o instanceof RequestResponseVO)
        {
            if (((RequestResponseVO) o).getCommand() == APIConstant.VERIFICATION_COMMAND_TAG)
            {
                if (((RequestResponseVO) o).getStatus() == APIConstant.SUCCESS)
                {
                    if (((UserVO) ((RequestResponseVO) o).getData()).getEmail() != null)
                    {
                        Util.insertUserData(this, (UserVO) ((RequestResponseVO) o).getData());
                        Intent intent = new Intent(this, KeyboardSetup.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.input_method_fancy_enter, R.anim.input_method_fancy_exit);
                        finish();
                    }
                    else
                    {
                        Util.showMessage(this, getString(R.string.error), getString(R.string.server_internal_error));
                    }
                }
                else if (((RequestResponseVO) o).getStatus() == APIConstant.DEACTIVATED_USER || ((RequestResponseVO) o).getStatus() == APIConstant.USER_ALREADY_REGISTER_OTHER_DEVICE)
                {
                    UserVO userVO = generateUser();
                    Util.deactivateOtherUser(this, userVO);
                }
                else if (((RequestResponseVO) o).getStatus() == APIConstant.NOT_REGISTERED)
                {
                    Util.showToastMessage(this, getString(R.string.warning), getString(R.string.user_not_register));
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(APIConstant.PAYMENT_URL));
                    startActivity(browserIntent);
                    overridePendingTransition(R.anim.input_method_fancy_enter, R.anim.input_method_fancy_exit);
                }
                else if (((RequestResponseVO) o).getStatus() == APIConstant.EXPIRED)
                {
                    setErrorStop();
                    Util.showMessage(this, getString(R.string.expiry_label), getString(R.string.account_expired));
                }
            }
            else if (((RequestResponseVO) o).getCommand() == APIConstant.DEACTIVATE_COMMAND_TAG)
            {
                if (((RequestResponseVO) o).getStatus() == APIConstant.SUCCESS)
                {
                    UserVO userVO = Util.getRegisterUserData(this);
                    if (null != userVO)
                    {
                        Util.deleteUserData(this, userVO);
                    }
                    Util.insertUserData(this, (UserVO) ((RequestResponseVO) o).getData());
                    Intent intent = new Intent(this, KeyboardSetup.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.input_method_fancy_enter, R.anim.input_method_fancy_exit);
                    finish();
                }
                else if (((RequestResponseVO) o).getStatus() == APIConstant.EXPIRED)
                {
                    setErrorStop();
                    Util.showMessage(this, getString(R.string.expiry_label), getString(R.string.account_expired));
                }
                else
                {
                    Util.showMessage(this, getString(R.string.error), getString(R.string.server_internal_error));
                }
            }
            else
            {
                Util.showMessage(this, getString(R.string.error), getString(R.string.server_internal_error));
            }
        }
        else if (o instanceof JSONException || o instanceof Exception)
        {
            showErrorMessage(((Exception) o).getMessage());
        }
    }

    public void verifyEmail(View view)
    {
        if (!Util.isNetworkAvailable(this))
        {
            Util.showMessage(this, getString(R.string.warning), getString(R.string.network_error));
            return;
        }
        if (!Util.isValidEmail(emailText.getText()))
        {
            emailText.setError(getString(R.string.error_invalid_email));
            return;
        }
        UserVO userVO = generateUser();
        String error = Util.validateUser(this, userVO);
        if (error != null)
        {
            Util.showMessage(this, getString(R.string.error), error);
            return;
        }
        changeState(false);
        start(processButton, handler);
        Cache.getInstance().setServiceCalled(false);

        CommunicationHandlerManager.getInstance().verifyUser(userVO);
    }

    private UserVO generateUser()
    {
        UserVO userVO = new UserVO();
        userVO.setEmail(emailText.getText().toString());
        userVO.setImei(Util.getIMEI(this));
        userVO.setFirstName(firstNameText.getText().toString());
        userVO.setLastName(lastNameText.getText().toString());
        userVO.setPhoneNumber(phoneNumberText.getText().toString());
        userVO.setDob(dobText.getText().toString());
        userVO.setAddress(addressText.getText().toString());
        userVO.setItsId(itsIdText.getText().toString());
        String gender = getString(R.string.male);
        if (femaleRadioButton.isChecked())
        {
            gender = getString(R.string.female);
        }
        userVO.setGender(gender);
        return userVO;
    }

    /**
     * Enable disable button
     *
     * @param state
     */
    private void changeState(boolean state)
    {
        emailText.setEnabled(state);
        firstNameText.setEnabled(state);
        lastNameText.setEnabled(state);
        phoneNumberText.setEnabled(state);
        dobText.setEnabled(state);
        addressText.setEnabled(state);
        processButton.setEnabled(state);
        maleRadioButton.setEnabled(state);
        femaleRadioButton.setEnabled(state);
        itsIdText.setEnabled(state);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, Startup.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.input_method_fancy_enter, R.anim.input_method_fancy_exit);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v
     *         The view that was clicked.
     */
    @Override
    public void onClick(View v)
    {
//        InputMethodManager imm = (InputMethodManager) getSystemService(
//                Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(dobText.getWindowToken(), 0);
//        datePicker.show();
    }
}
