package com.burhan.soft.lisanuddawat.ui;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;

import com.burhan.soft.lisanuddawat.R;
import com.lisanuddawat.Configuration;
import com.lisanuddawat.ConfigurationImpl;
import com.lisanuddawat.devicespecific.DeviceSpecific;
import com.lisanuddawat.devicespecific.FactoryViewBase;

public class LisanApplication extends Application implements OnSharedPreferenceChangeListener {

    public static final boolean DEBUG = false;
    //public static final boolean BLEEDING_EDGE = false;

    private static final String TAG = "ASK_APP";
    private static Configuration msConfig;
    private static DeviceSpecific msDeviceSpecific;

    private static boolean isEnglish;

    public static boolean isIsEnglish() {
        return isEnglish;
    }

    public static void setIsEnglish(boolean isEnglish) {
        LisanApplication.isEnglish = isEnglish;
    }

    private static Handler handler;

    public static Handler getHandler() {
        return handler;
    }

    public static void setHandler(Handler handler) {
        LisanApplication.handler = handler;
    }

    private static Handler serviceHandler;

    public static Handler getServiceHandler() {
        return serviceHandler;
    }

    public static void setServiceHandler(Handler serviceHandler) {
        LisanApplication.serviceHandler = serviceHandler;
    }

    private static boolean symbol;

    public static boolean isSymbol() {
        return symbol;
    }

    public static void setSymbol(boolean symbol) {
        LisanApplication.symbol = symbol;
    }

    @Override
    public void onCreate() {
//		if (DEBUG) {
//			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//				.detectAll()
//				.penaltyLog()
//				.build());
//			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//				.detectAll()
//				.penaltyLog()
//				.penaltyDeath()
//				.build());
//		}
        super.onCreate();

        if (DEBUG) Log.d(TAG, "** Starting application in DEBUG mode.");

        msConfig = new ConfigurationImpl(this);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sp.registerOnSharedPreferenceChangeListener(this);

        LayoutInflater inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        FactoryViewBase factory = (FactoryViewBase) inflate.inflate(R.layout.device_specific, null);
        msDeviceSpecific = factory.createDeviceSpecific();
        factory = null;//GC! Please clean this view!

    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        ((ConfigurationImpl) msConfig).onSharedPreferenceChanged(sharedPreferences, key);
//        //should we disable the Settings App? com.burhan.soft.lisanuddawat.LauncherSettingsActivity
//        if (key.equals(getString(R.string.settings_key_show_settings_app))) {
//            PackageManager pm = getPackageManager();
//            boolean showApp = sharedPreferences.getBoolean(key, getResources().getBoolean(R.bool.settings_default_show_settings_app));
//            pm.setComponentEnabledSetting(new ComponentName(getApplicationContext(), com.burhan.soft.lisanuddawat.LauncherSettingsActivity.class),
//                    showApp ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
//                    PackageManager.DONT_KILL_APP);
//        }
    }


    public static Configuration getConfig() {
        return msConfig;
    }

    public static DeviceSpecific getDeviceSpecific() {
        return msDeviceSpecific;
    }

    public static void requestBackupToCloud() {
    }

}
