package com.burhan.soft.lisanuddawat.ui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;
import com.burhan.soft.lisanuddawat.service.VerificationService;
import com.burhan.soft.lisanuddawat.util.Util;
import com.burhan.soft.lisanuddawat.widget.LisanWidget;
import com.viewpagerindicator.CirclePageIndicator;

import pl.droidsonroids.gif.GifImageView;


public class Startup extends AbstractUIActivity
{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     */
    SectionsPagerAdapter mSectionsPagerAdapter;
    private MediaPlayer player;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    private CirclePageIndicator circlePageIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent();
        intent.setAction(LisanWidget.WIDGET_TEXT_CHANGE_ACTION);
        sendBroadcast(intent);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.abc_fade_in);
        animation.setDuration(500);

        mViewPager.setPageTransformer(true, new ViewPager.PageTransformer()
        {
            @Override
            public void transformPage(View view, float position)
            {
                // view.startAnimation(animation);
            }
        });

        circlePageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        circlePageIndicator.setViewPager(mViewPager);

        circlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {

            @Override
            public void onPageSelected(int position)
            {
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        if (!Util.isServiceRunning(this, VerificationService.class))
        {
            Intent service = new Intent(this, VerificationService.class);
            startService(service);
        }

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

    }

    @Override
    protected void onResume()
    {
        super.onResume();
//        player = MediaPlayer.create(this, R.raw.bg_sound);
//        player.setLooping(true); // Set looping
//        player.setVolume(100, 100);
//        player.start();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
//        if(player != null) {
//            player.stop();
//            player = null;
//        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
//        if(player != null) {
//            player.stop();
//            player = null;
//        }
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    protected int getLayoutResource()
    {
        return R.layout.activity_startup;
    }

    /**
     * Handle messages received from the handler
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message)
    {

    }

    public void skipIntro(View view)
    {
        UserVO userVO = Util.getRegisterUserData(this);
        if (userVO != null && userVO.getExpiry() != APIConstant.APP_EXPIRED)
        {
            Intent intent = new Intent(this, KeyboardSetup.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(this, VerificationActivity.class);
            intent.putExtra(APIConstant.IS_NOT_REGISTERED, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (null != userVO)
            {
                if (userVO.getExpiry() == APIConstant.APP_EXPIRED)
                {
                    intent.putExtra(APIConstant.DEACTIVATE_USER_BY_OTHER, true);
                    intent.putExtra(APIConstant.DEACTIVATED_USER_EMAIL, userVO.getEmail());
                    intent.putExtra(APIConstant.ERROR_MESSAGE, getString(R.string.account_expired));
                }
            }
            startActivity(intent);
        }
        finish();
        overridePendingTransition(R.anim.input_method_fancy_enter, R.anim.input_method_fancy_exit);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter
    {

        public SectionsPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }


        @Override
        public Fragment getItem(int position)
        {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount()
        {
            // Show 8 total pages.
            return 6;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return "";
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
    {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int section;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber)
        {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment()
        {
        }

        int imageArray[] = {R.drawable.alphabets, R.drawable.alphabets_suggestions, R.drawable.animated_widget, R.drawable.alphabet_popup, R.drawable.symbol_popup, R.drawable.keyboard_message};

        int titleArray[] = {R.string.introduction_1_title, R.string.introduction_2_title, R.string.introduction_8_title, R.string.introduction_5_title, R.string.introduction_6_title, R.string.introduction_7_title};

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
        {
            section = getArguments().getInt(ARG_SECTION_NUMBER);
            View rootView = inflater.inflate(R.layout.fragment_startup, container, false);
            GifImageView imageView = (GifImageView) rootView.findViewById(R.id.feature_image);
            imageView.setImageResource(imageArray[section]);

            TextView title = (TextView) rootView.findViewById(R.id.feature_title);
            title.setText(getString(titleArray[section]));
            TextView description = (TextView) rootView.findViewById(R.id.feature_description);

            description.setText(getString(R.string.introduction_description));
            return rootView;
        }

        public static class ViewHolder
        {
            public GifImageView gifImageView;
        }
    }
}
