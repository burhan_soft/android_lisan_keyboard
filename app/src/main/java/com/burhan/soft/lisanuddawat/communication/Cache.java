package com.burhan.soft.lisanuddawat.communication;

import android.os.IBinder;

import com.burhan.soft.lisanuddawat.communication.data.UserVO;

/**
 * Created by TAYEBT on 5/4/2015.
 */
public class Cache {

    private static Cache instance = new Cache();

    private Cache() {
    }

    public static Cache getInstance() {
        return instance;
    }

    private UserVO userVO;

    public UserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    private boolean serviceCalled;

    public boolean isServiceCalled() {
        return serviceCalled;
    }

    public void setServiceCalled(boolean serviceCalled) {
        this.serviceCalled = serviceCalled;
    }

    private boolean vibrationOn;
    private boolean soundOn;

    public boolean isVibrationOn() {
        return vibrationOn;
    }

    public void setVibrationOn(boolean vibrationOn) {
        this.vibrationOn = vibrationOn;
    }

    public boolean isSoundOn() {
        return soundOn;
    }

    public void setSoundOn(boolean soundOn) {
        this.soundOn = soundOn;
    }

    private String productAmount;

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    private IBinder binder;

    public IBinder getBinder() {
        return binder;
    }

    public void setBinder(IBinder binder) {
        this.binder = binder;
    }
}
