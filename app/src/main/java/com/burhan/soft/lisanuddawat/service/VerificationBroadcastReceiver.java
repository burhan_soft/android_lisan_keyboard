package com.burhan.soft.lisanuddawat.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.data.RequestResponseVO;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;
import com.burhan.soft.lisanuddawat.ui.VerificationActivity;
import com.burhan.soft.lisanuddawat.util.Util;

import org.json.JSONException;

/**
 * Created by TAYEBT on 5/4/2015.
 */
public class VerificationBroadcastReceiver extends BroadcastReceiver
{

    private int notificationID = 100;

    /**
     * This method is called when the BroadcastReceiver is receiving an Intent
     * broadcast.  During this time you can use the other methods on
     * BroadcastReceiver to view/modify the current result values.  This method
     * is always called within the main thread of its process, unless you
     * explicitly asked for it to be scheduled on a different thread using
     * {@link android.content.Context#registerReceiver(android.content.BroadcastReceiver,
     * android.content.IntentFilter, String, android.os.Handler)}. When it runs on the main
     * thread you should
     * never perform long-running operations in it (there is a timeout of
     * 10 seconds that the system allows before considering the receiver to
     * be blocked and a candidate to be killed). You cannot launch a popup dialog
     * in your implementation of onReceive().
     * <p/>
     * <p><b>If this BroadcastReceiver was launched through a &lt;receiver&gt; tag,
     * then the object is no longer alive after returning from this
     * function.</b>  This means you should not perform any operations that
     * return a result to you asynchronously -- in particular, for interacting
     * with services, you should use
     * {@link android.content.Context#startService(android.content.Intent)} instead of
     * {@link android.content.Context#bindService(android.content.Intent, android.content.ServiceConnection, int)} (android.content.Intent, ServiceConnection, int)}.  If you wish
     * to interact with a service that is already running, you can use
     * {@link #peekService}.
     * <p/>
     * <p>The Intent filters used in {@link android.content.Context#registerReceiver}
     * and in application manifests are <em>not</em> guaranteed to be exclusive. They
     * are hints to the operating system about how to find suitable recipients. It is
     * possible for senders to force delivery to specific recipients, bypassing filter
     * resolution.  For this reason, {@link #onReceive(android.content.Context, android.content.Intent) onReceive()}
     * implementations should respond only to known actions, ignoring any unexpected
     * Intents that they may receive.
     *
     * @param context
     *         The Context in which the receiver is running.
     * @param intent
     *         The Intent being received.
     */
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Object o = intent.getSerializableExtra(VerificationService.RESULT);
        processResponse(o, context);
    }

    private void processResponse(Object o, Context context)
    {
        if (o instanceof RequestResponseVO)
        {
            if (APIConstant.VERIFICATION_COMMAND_TAG.equalsIgnoreCase(((RequestResponseVO) o).getCommand()))
            {
                if (((RequestResponseVO) o).getStatus() == APIConstant.DEACTIVATED_USER || ((RequestResponseVO) o).getStatus() == APIConstant.USER_ALREADY_REGISTER_OTHER_DEVICE || ((RequestResponseVO) o).getStatus() == APIConstant.EXPIRED)
                {
                    UserVO userVO = (UserVO) ((RequestResponseVO) o).getData();
                    Intent intent = new Intent(context, VerificationActivity.class);
                    intent.putExtra(APIConstant.DEACTIVATE_USER_BY_OTHER, true);
                    intent.putExtra(APIConstant.IS_NOT_REGISTERED, true);
                    intent.putExtra(APIConstant.DEACTIVATED_USER_EMAIL, ((UserVO) ((RequestResponseVO) o).getData()).getEmail());
                    if (userVO.getExpiry() == APIConstant.APP_EXPIRED)
                    {
                        Util.insertUserData(context, userVO);
                        intent.putExtra(APIConstant.ERROR_MESSAGE, context.getString(R.string.account_expired));
                    }
                    else
                    {
                        Util.deleteUserData(context, userVO);
                        intent.putExtra(APIConstant.ERROR_MESSAGE, context.getString(R.string.account_deactivate_by_other));
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                else if (((UserVO) ((RequestResponseVO) o).getData()).getExpiry() != APIConstant.APP_NOT_EXPIRED && ((UserVO) ((RequestResponseVO) o).getData()).getExpiry() != 0)
                {
                    UserVO userVO = Util.getRegisterUserData(context);
                    if (userVO != null && userVO.getExpiry() != ((UserVO) ((RequestResponseVO) o).getData()).getExpiry())
                    {
                        NotificationManager mNotificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(APIConstant.PAYMENT_URL));
                        PendingIntent contentIntent = PendingIntent.getActivity(context, 1, browserIntent, 0);

                        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                                context);
                        Notification notification = builder.setContentIntent(contentIntent)
                                .setSmallIcon(R.drawable.ic_launcher).setTicker(context.getString(R.string.app_name)).setWhen(0)
                                .setAutoCancel(true).setContentTitle(context.getString(R.string.expiry_label))
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(context.getString(R.string.account_going_to_expire)))
                                .setContentText(context.getString(R.string.account_going_to_expire)).build();


                        notification.defaults |= Notification.DEFAULT_LIGHTS;
                        notification.defaults |= Notification.FLAG_AUTO_CANCEL;
                        notification.flags = Notification.DEFAULT_LIGHTS
                                | Notification.FLAG_AUTO_CANCEL;
                    /* Update the existing notification using same notification ID */
                        mNotificationManager.notify(notificationID, notification);
                        Util.insertUserData(context, userVO);
                    }
                }
            }
        }
        else if (o instanceof JSONException || o instanceof Exception)
        {
            Util.showToastMessage(context, context.getString(R.string.error), ((Exception) o).getMessage());
        }
    }
}
