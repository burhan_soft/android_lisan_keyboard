package com.burhan.soft.lisanuddawat.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.burhan.soft.lisanuddawat.R;

/**
 * Implementation of App Widget functionality.
 */
public class LisanWidget extends AppWidgetProvider {

    public static final String WIDGET_ACTION = "com.burhan.soft.intent.action.LANGUAGE";

    public static final String WIDGET_TEXT_CHANGE_ACTION = "com.burhan.soft.intent.action.TEXT_CHNAGE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.lisan_widget);
        remoteViews.setOnClickPendingIntent(R.id.languageChangeBtn, buildButtonPendingIntent(context));
        pushWidgetUpdate(context, remoteViews);
    }

    public static PendingIntent buildButtonPendingIntent(Context context) {
        Intent intent = new Intent();
        intent.setAction(WIDGET_ACTION);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        ComponentName myWidget = new ComponentName(context, LisanWidget.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        manager.updateAppWidget(myWidget, remoteViews);
    }
}


