package com.burhan.soft.lisanuddawat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.data.PaymentVO;
import com.burhan.soft.lisanuddawat.util.Util;

public class InvoiceActivity extends AbstractUIActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        PaymentVO paymentVO = (PaymentVO) intent.getSerializableExtra(APIConstant.INVOICE_DATA_KEY);
        if (null != paymentVO) {
            Util.showToastMessage(this, getString(R.string.info), paymentVO.toString());
        }
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_invoice;
    }

    /**
     * Handle messages received from the handler
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }
}
