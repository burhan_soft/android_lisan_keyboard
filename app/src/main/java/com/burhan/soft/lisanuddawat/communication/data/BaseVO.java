package com.burhan.soft.lisanuddawat.communication.data;

import java.io.Serializable;

/**
 * Created by TAYEBT on 5/2/2015.
 */
public class BaseVO implements Serializable {

    private int statusCode;
    private String errorMessage;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
