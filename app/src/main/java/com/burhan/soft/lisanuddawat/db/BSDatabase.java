package com.burhan.soft.lisanuddawat.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.widget.Toast;

import com.lisanuddawat.LisanSoftKeyboard;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by sandip.mahajan on 4/5/2015.
 */
public class BSDatabase {

    public static final String KEY_WORD = "word_text";
    public static final String KEY_WORD_ENGLISH = "word_english";
    public static final String KEY_ID = "_id";
    public static final String PREDICTION_KEY_WORD = "word";
    public static final String PREDICTION_KEY_WORD_DESCRIPTION = "prediction";
    public static final String PREDICTION_KEY_ID = "_id";

    public static final String USER_KEY_IMEI = "imei";
    public static final String USER_KEY_EMAIL = "email";
    public static final String USER_KEY_TRANSACTION_ID = "transid";
    public static final String USER_KEY_TRANSACTION_DATE = "transdate";
    public static final String USER_KEY_STATUS = "status";
    public static final String USER_KEY_FIRST_NAME = "fname";
    public static final String USER_KEY_LAST_NAME = "lname";
    public static final String USER_KEY_ADDRESS = "address";
    public static final String USER_KEY_DATE_OF_BIRTH = "dob";
    public static final String USER_KEY_PHONE_NUMBER = "phone";
    public static final String USER_KEY_AMOUNT = "amount";
    public static final String USER_KEY_BANK_CODE = "bankcode";
    public static final String USER_KEY_CARD_NUMBER = "cnumber";
    public static final String USER_KEY_MODE = "mode";
    public static final String USER_KEY_EXPIRY = "expiry";
    public static final String USER_KEY_ITS_ID = "itsid";

    private static final String BURHAN_TABLE = "tblburhan";
    private static final String PREDICTION_BURHAN_TABLE = "tblpredictions";
    private static final String USER_BURHAN_TABLE = "tbluser";


    private static final HashMap<String, String> mColumnMap = buildColumnMap();
    private final BSDBHelper bsdbHelper;
    private Context mContext;

    /**
     * Initialize database helper
     *
     * @param context
     */
    public BSDatabase(Context context) {
        mContext = context;
        this.bsdbHelper = new BSDBHelper(context);
    }

    /**
     * Create column map to get data of specific columns
     *
     * @return
     */
    private static HashMap<String, String> buildColumnMap() {
        HashMap localHashMap = new HashMap();
        localHashMap.put(KEY_WORD, KEY_WORD);
        localHashMap.put(KEY_WORD_ENGLISH, KEY_WORD_ENGLISH);
        localHashMap.put(KEY_ID, "rowid AS _id");
        return localHashMap;
    }

    public void createDatabase() throws SQLException {
        try {
            bsdbHelper.createDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToCreateDatabase");
        }
    }

    public void open() throws SQLException {

        try {
            boolean isOpen = bsdbHelper.openDataBase();
            if (isOpen) {
                bsdbHelper.getWritableDatabase();
            } else {
                Toast.makeText(mContext, "Database not open", Toast.LENGTH_LONG).show();
            }
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }
    }

    public void close() {
        bsdbHelper.close();
    }

    /**
     * Fire query to get data from database
     *
     * @param paramString
     * @param projections
     * @param selections
     * @return
     */
    private Cursor query(String paramString, String[] projections, String[] selections, String tableName) {

        if (bsdbHelper.getDatabase() != null) {
            Cursor cursor = bsdbHelper.getDatabase().query(true, tableName, projections, paramString, selections, null, null, null, LisanSoftKeyboard.SEARCH_WORD_COUNT + "");
            if (cursor == null) {
                return null;
            }

            if (!cursor.moveToFirst()) {
                cursor.close();
                return null;
            }

            return cursor;
        }
        return null;
    }

    /**
     * Get matching word, it will result into single or multiple words
     *
     * @param paramString
     * @param paramArrayOfString
     * @return
     */
    public Cursor getWord(String paramString, String[] paramArrayOfString) {
        return query("rowid = ?", new String[]{paramString}, paramArrayOfString, BURHAN_TABLE);
    }

    /**
     * Get all world matches from database using <input>% like query
     *
     * @param paramString
     * @param projections
     * @return
     */
    public Cursor getWordMatches(String paramString, String[] projections) {
        String[] selectionStrings = new String[1];
        selectionStrings[0] = paramString + "%";
        for (String str = KEY_WORD + " LIKE ? "; ; ) {
            return query(str, projections, selectionStrings, BURHAN_TABLE);
        }
    }

    /**
     * Get all world matches from database using <input> = query
     *
     * @param paramString
     * @param projections
     * @return
     */
    public Cursor getWordEquals(String paramString, String[] projections) {
        String[] selectionStrings = new String[1];
        selectionStrings[0] = paramString;
        for (String str = PREDICTION_KEY_WORD + " = ? "; ; ) {
            return query(str, projections, selectionStrings, PREDICTION_BURHAN_TABLE);
        }
    }

    /**
     * Get the user record matches from database using <input> = query
     *
     * @param paramString
     * @param projections
     * @return
     */
    public Cursor getUserRecord(String paramString, String[] projections) {
        String[] selectionStrings = new String[1];
        selectionStrings[0] = paramString;
        for (String str = USER_KEY_IMEI + " = ? "; ; ) {
            return query(str, projections, selectionStrings, USER_BURHAN_TABLE);
        }
    }

    public long insertUserData(ContentValues values) {
        if (bsdbHelper.getDatabase() != null) {
            return bsdbHelper.getDatabase().insert(USER_BURHAN_TABLE, null, values);
        }
        return -1;
    }

    public int deleteUserData(String paramString) {
        String[] selectionStrings = new String[1];
        selectionStrings[0] = paramString;
        String condition = USER_KEY_EMAIL + " = ? ";
        if (bsdbHelper.getDatabase() != null) {
            return bsdbHelper.getDatabase().delete(USER_BURHAN_TABLE, null, null);
        }
        return -1;
    }
}
