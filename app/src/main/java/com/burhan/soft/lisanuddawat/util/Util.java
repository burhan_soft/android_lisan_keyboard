package com.burhan.soft.lisanuddawat.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.Cache;
import com.burhan.soft.lisanuddawat.communication.business.CommunicationHandlerManager;
import com.burhan.soft.lisanuddawat.communication.data.PaymentVO;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;
import com.burhan.soft.lisanuddawat.db.BSDBProvider;
import com.burhan.soft.lisanuddawat.db.BSDatabase;
import com.burhan.soft.lisanuddawat.db.DBConstant;
import com.burhan.soft.lisanuddawat.service.BaseService;
import com.burhan.soft.lisanuddawat.ui.AbstractUIActivity;
import com.burhan.soft.lisanuddawat.ui.LisanApplication;

import java.lang.ref.WeakReference;
import java.util.Random;

/**
 * Created by TAYEBT on 5/3/2015.
 */
public class Util {

    public static void showToastMessage(Context context, String title, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showMessage(Context context, String title, String message) {
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(context);
        materialDialog.title(title)
                .content(message)
                .positiveText(context.getString(R.string.okay_button))
                .show();
        materialDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {

            }

            @Override
            public void onNegative(MaterialDialog dialog) {
            }
        });
    }


    public static boolean isThisKeyboardSetAsDefaultIME(Context context) {
        final String defaultIME = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
        return isThisKeyboardSetAsDefaultIME(defaultIME, context.getPackageName());
    }

    private static boolean isThisKeyboardSetAsDefaultIME(String defaultIME, String myPackageName) {
        if (TextUtils.isEmpty(defaultIME))
            return false;

        ComponentName defaultInputMethod = ComponentName.unflattenFromString(defaultIME);
        if (defaultInputMethod.getPackageName().equals(myPackageName)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isThisKeyboardEnabled(Context context) {
        final String enabledIMEList = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_INPUT_METHODS);
        return isThisKeyboardEnabled(enabledIMEList, context.getPackageName());
    }

    public static boolean isThisKeyboardEnabled(String enabledIMEList, String myPackageName) {
        if (TextUtils.isEmpty(enabledIMEList))
            return false;

        String[] enabledIMEs = enabledIMEList.split(":");
        for (String enabledIMEId : enabledIMEs) {
            ComponentName enabledIME = ComponentName.unflattenFromString(enabledIMEId);
            if (enabledIME != null && enabledIME.getPackageName().equals(myPackageName)) {
                return true;
            }
        }

        return false;
    }

    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    public static int randomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    /**
     * This will register handler for each activity
     *
     * @param context
     */
    public static Handler registerWeakHandler(AbstractUIActivity context) {
        return new UIhandler(context);
    }

    /**
     * Weak reference handler to remove handler leaks
     * Created by sandip.mahajan on 1/24/2015.
     */
    private static class UIhandler extends Handler {
        private final WeakReference<AbstractUIActivity> reference;

        public UIhandler(AbstractUIActivity activity) {
            reference = new WeakReference<AbstractUIActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            AbstractUIActivity activity = reference.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }

    public static void sendResponseMessage(String what, Object obj) {
        Handler handler = null;
        Handler serviceHandler = null;
        if (Cache.getInstance().isServiceCalled()) {
            handler = LisanApplication.getServiceHandler();
        } else {
            handler = LisanApplication.getHandler();
        }
        if (null != handler) {
            Message message = new Message();
            message.what = Integer.parseInt(what);
            if (null != obj)
                message.obj = obj;
            handler.sendMessage(message);
        }
    }

    public static UserVO getRegisterUserData(Context context) {
        try {
            Uri uri = BSDBProvider.CONTENT_URI;
            String imei = Util.getIMEI(context);
            Cursor cursor = context.getContentResolver().query(uri, null, imei, null,
                    DBConstant.SEARCH_USER_RECORD);
            UserVO userVO = null;
            if (cursor != null) {
                cursor.moveToFirst();
                userVO = new UserVO();
                userVO.setId(cursor.getInt(cursor.getColumnIndexOrThrow(BSDatabase.KEY_ID)));
                userVO.setEmail(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_EMAIL)));
                userVO.setImei(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_IMEI)));
                userVO.setTransactionId(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_TRANSACTION_ID)));
                userVO.setTransactionDate(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_TRANSACTION_DATE)));
                userVO.setStatus(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_STATUS)));
                userVO.setFirstName(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_FIRST_NAME)));
                userVO.setLastName(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_LAST_NAME)));
                userVO.setAddress(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_ADDRESS)));
                userVO.setAmount(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_AMOUNT)));
                userVO.setDob(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_DATE_OF_BIRTH)));
                userVO.setPhoneNumber(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_PHONE_NUMBER)));
                userVO.setBankCode(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_BANK_CODE)));
                userVO.setCardNumber(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_CARD_NUMBER)));
                userVO.setMode(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_MODE)));
                userVO.setExpiry(cursor.getInt(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_EXPIRY)));
                userVO.setItsId(cursor.getString(cursor.getColumnIndexOrThrow(BSDatabase.USER_KEY_ITS_ID)));
                Cache.getInstance().setUserVO(userVO);
            }
            return userVO;
        } catch (Exception e) {
            Log.e("", e.getMessage());
        }
        return null;
    }

    public static void deactivateOtherUser(final Context context, final UserVO userVO) {
        final MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(context);
        materialDialog.title(R.string.confirmation)
                .content(context.getString(R.string.application_deactivate_confirmation))
                .positiveText(context.getString(R.string.yes_button))
                .negativeText(context.getString(R.string.no_button))
                .show();
        materialDialog.callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                // Deactivate old account
                if (!Util.isNetworkAvailable(context)) {
                    Util.showMessage(context, context.getString(R.string.warning), context.getString(R.string.network_error));
                    return;
                }
                showProgressBar(context);
                CommunicationHandlerManager.getInstance().deactivateUser(userVO);
            }

            @Override
            public void onNegative(MaterialDialog dialog) {

            }
        });
    }

    public static boolean insertTransactionData(Context context, PaymentVO paymentVO) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(APIConstant.EMAIL, paymentVO.getEmail());
        contentValues.put(APIConstant.IMEI, paymentVO.getImei());
        contentValues.put(APIConstant.TRANSACTION_ID, paymentVO.getTransactionId());
        contentValues.put(APIConstant.TRANSACTION_DATE, paymentVO.getTransactionDate());
        contentValues.put(APIConstant.PAYMENT_STATUS, paymentVO.getStatus());
        Uri uri = BSDBProvider.CONTENT_URI;
        Uri cursor = context.getContentResolver().insert(uri, contentValues);
        return true;
    }

    public static boolean insertUserData(Context context, UserVO userVO) {
        deleteUserData(context, userVO);
        ContentValues contentValues = new ContentValues();
        contentValues.put(BSDatabase.USER_KEY_EMAIL, userVO.getEmail());
        contentValues.put(BSDatabase.USER_KEY_IMEI, userVO.getImei());
        contentValues.put(BSDatabase.USER_KEY_TRANSACTION_ID, userVO.getTransactionId());
        contentValues.put(BSDatabase.USER_KEY_TRANSACTION_DATE, userVO.getTransactionDate());
        contentValues.put(BSDatabase.USER_KEY_STATUS, userVO.getStatus());
        contentValues.put(BSDatabase.USER_KEY_FIRST_NAME, userVO.getFirstName());
        contentValues.put(BSDatabase.USER_KEY_LAST_NAME, userVO.getLastName());
        contentValues.put(BSDatabase.USER_KEY_DATE_OF_BIRTH, userVO.getDob());
        contentValues.put(BSDatabase.USER_KEY_ADDRESS, userVO.getAddress());
        contentValues.put(BSDatabase.USER_KEY_PHONE_NUMBER, userVO.getPhoneNumber());
        contentValues.put(BSDatabase.USER_KEY_BANK_CODE, userVO.getBankCode());
        contentValues.put(BSDatabase.USER_KEY_AMOUNT, userVO.getAmount());
        contentValues.put(BSDatabase.USER_KEY_CARD_NUMBER, userVO.getCardNumber());
        contentValues.put(BSDatabase.USER_KEY_MODE, userVO.getMode());
        contentValues.put(BSDatabase.USER_KEY_EXPIRY, userVO.getExpiry());
        contentValues.put(BSDatabase.USER_KEY_ITS_ID, userVO.getItsId());
        Uri uri = BSDBProvider.CONTENT_URI;
        Uri cursor = context.getContentResolver().insert(uri, contentValues);
        return true;
    }

    public static boolean deleteUserData(Context context, UserVO userVO) {
        Uri uri = BSDBProvider.CONTENT_URI;
        int result = context.getContentResolver().delete(uri, userVO.getEmail() + "", null);
        return result != -1;
    }

    public static class ServiceHandler extends Handler {
        private final WeakReference<BaseService> reference;

        public ServiceHandler(BaseService service) {
            reference = new WeakReference<BaseService>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            BaseService service = reference.get();
            if (service != null) {
                service.handleMessage(msg);
            }
        }
    }

    public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    public static void setSettingsParameters(Context context) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        Cache.getInstance().setVibrationOn(SP.getBoolean(context.getString(R.string.settings_key_vibrate_on_key_press_duration), true));
        Cache.getInstance().setSoundOn(SP.getBoolean(context.getString(R.string.settings_key_sound_on), true));
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void stopRunningService(Context context, Class<?> serviceClass) {
        Intent intent = new Intent(context, serviceClass);
        if (isServiceRunning(context, serviceClass)) {
            context.stopService(intent);
        }
        context.startService(intent);
    }

    private static ProgressDialog progressDialog;

    public static void showProgressBar(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void stopProgressBar() {
        if (null != progressDialog) {
            progressDialog.dismiss();
        }
    }

    public static String validateUser(Context context, UserVO userVO) {
        if (userVO.getFirstName() == null || "".equalsIgnoreCase(userVO.getFirstName()))
            return context.getString(R.string.empty_first_name);
        else if (userVO.getFirstName().length() > 50)
            return context.getString(R.string.length_first_name);
        else if (userVO.getLastName() == null || "".equalsIgnoreCase(userVO.getLastName()))
            return context.getString(R.string.empty_last_name);
        else if (userVO.getLastName().length() > 50)
            return context.getString(R.string.length_last_name);
        else if (userVO.getPhoneNumber() == null || "".equalsIgnoreCase(userVO.getPhoneNumber()))
            return context.getString(R.string.empty_phone_number);
        else if (userVO.getPhoneNumber().length() > 15)
            return context.getString(R.string.length_phone_number);
        else if (userVO.getDob() == null || "".equalsIgnoreCase(userVO.getDob()))
            return context.getString(R.string.empty_dob);
        else if (userVO.getAddress() == null || "".equalsIgnoreCase(userVO.getAddress()))
            return context.getString(R.string.empty_address);
        else if (userVO.getAddress().length() > 100)
            return context.getString(R.string.length_address);
        else if (userVO.getItsId() == null || "".equalsIgnoreCase(userVO.getItsId()))
            return context.getString(R.string.empty_its_id);
        else if (userVO.getItsId().length() > 10)
            return context.getString(R.string.length_its_id);;
        return null;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
