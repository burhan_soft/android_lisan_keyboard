package com.burhan.soft.lisanuddawat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.JavaScriptHandler;
import com.burhan.soft.lisanuddawat.communication.data.PaymentVO;
import com.burhan.soft.lisanuddawat.communication.data.RequestResponseVO;
import com.burhan.soft.lisanuddawat.communication.response.ResponseParser;
import com.burhan.soft.lisanuddawat.util.Util;

import org.json.JSONException;

public class PaymentActivity extends AbstractUIActivity {

    private WebView webView;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        email = intent.getStringExtra(APIConstant.EMAIL);
        webView = (WebView) this.findViewById(R.id.paypalview);
        setTitle(getString(R.string.payment));
        handleWebView();
    }

    private void handleWebView() {
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewController());
        openURL();
        webView.addJavascriptInterface(new JavaScriptHandler(this), "LisanHandler");
    }

    /**
     * Opens the URL in a browser
     */
    private void openURL() {
        webView.loadUrl(APIConstant.PAYMENT_URL + "?" + APIConstant.EMAIL + "=" + email + "&" + APIConstant.IMEI + "=" + Util.getIMEI(this));
        webView.requestFocus();
    }

    public void javascriptCallFinished(final String response) {
        // I need to run set operation of UI on the main thread.
        // therefore, the above parameter "val" must be final
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    RequestResponseVO requestResponseVO = ResponseParser.getInstance().parse(response);
                    Message message = new Message();
                    message.obj = requestResponseVO;
                    handleMessage(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Util.sendResponseMessage(APIConstant.ERROR + "", e);
                    // Show error message
                }
            }
        });
    }

    public class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_payment;
    }

    /**
     * Handle messages received from the handler
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {
        Object o = message.obj;
        if (((RequestResponseVO) o).getCommand() == APIConstant.PAYPAL_RESPONSE_TAG) {
            if (((RequestResponseVO) o).getStatus() == APIConstant.SUCCESS) {
                PaymentVO paymentVO = (PaymentVO) ((RequestResponseVO) o).getData();
                processPayPalData(paymentVO);
            } else {
                showErrorMessage((String) ((RequestResponseVO) o).getData());
            }
        }
    }

    private void processPayPalData(PaymentVO paymentVO) {
        try {
            Util.insertTransactionData(this, paymentVO);
            Intent invoiceIntent = new Intent(this, KeyboardSetup.class);
            startActivity(invoiceIntent);
            overridePendingTransition(R.anim.input_method_fancy_enter, R.anim.input_method_fancy_exit);
            finish();
        } catch (Exception exception) {
            showErrorMessage(exception.getMessage());
        }
    }
}
