package com.burhan.soft.lisanuddawat.service;

import android.os.Message;

/**
 * Created by TAYEBT on 5/4/2015.
 */
public interface BaseService {

    void handleMessage(Message msg);
}
