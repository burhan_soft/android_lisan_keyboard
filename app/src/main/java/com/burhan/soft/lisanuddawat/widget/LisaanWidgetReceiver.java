package com.burhan.soft.lisanuddawat.widget;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.provider.Settings;
import android.util.Log;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.RemoteViews;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.Cache;
import com.burhan.soft.lisanuddawat.util.Util;

import java.util.List;

public class LisaanWidgetReceiver extends BroadcastReceiver {

    private Context context;


    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        context.getContentResolver().unregisterContentObserver(settingsChanged);
        context.getContentResolver().registerContentObserver(Settings.Secure.CONTENT_URI, true, settingsChanged);
        if (intent.getAction().equals(LisanWidget.WIDGET_ACTION)) {
            updateWidgetChangeLanguageListener(context);
        } else if (intent.getAction().equals(LisanWidget.WIDGET_TEXT_CHANGE_ACTION)) {
            changeWidgetText(context);
        }
    }

    private void updateWidgetChangeLanguageListener(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.lisan_widget);
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showInputMethodPicker();
        } catch (Throwable t) { // java.lang.NoSuchMethodError if API_level<11
            Log.e("", "cannot set the previous input method:");
            t.printStackTrace();
        }
        remoteViews.setOnClickPendingIntent(R.id.languageChangeBtn, LisanWidget.buildButtonPendingIntent(context));
        LisanWidget.pushWidgetUpdate(context.getApplicationContext(), remoteViews);
    }

    private void changeWidgetText(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.lisan_widget);
//        try {
//            if (Util.isThisKeyboardSetAsDefaultIME(context)) {
//                remoteViews.setTextViewText(R.id.appwidget_text, "Lisaan-ud-Dawat");
//            } else {
//                remoteViews.setTextViewText(R.id.appwidget_text, "Other");
//            }
//        } catch (Throwable t) { // java.lang.NoSuchMethodError if API_level<11
//            Log.e("", "cannot set the previous input method:");
//            t.printStackTrace();
//        }
        LisanWidget.pushWidgetUpdate(context.getApplicationContext(), remoteViews);
    }

    private final ContentObserver settingsChanged = new ContentObserver(null) {
        @Override
        public boolean deliverSelfNotifications() {
            return false;
        }

        @Override
        public void onChange(boolean selfChange) {
            changeWidgetText(context);
        }
    };
}
