package com.burhan.soft.lisanuddawat.communication.request;

import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by TAYEBT on 5/2/2015.
 */
public class RequestGenerator
{

    private static RequestGenerator requestGenerator = new RequestGenerator();

    private RequestGenerator()
    {
    }

    public static RequestGenerator getInstance()
    {
        return requestGenerator;
    }

    /**
     * Generate common request
     *
     * @param tag
     * @param map
     *
     * @return
     */
    private JSONObject generateRequest(String tag, Map<String, String> map)
    {
        map.put(APIConstant.TAG, tag + "");
        return new JSONObject(map);
    }

    /**
     * Generate verification account request
     *
     * @param userVO
     *
     * @return
     */
    public JSONObject generateVerifyRequest(UserVO userVO)
    {
        Map<String, String> map = generateRequest(userVO);
        return generateRequest(APIConstant.VERIFICATION_COMMAND_TAG, map);
    }

    /*-*
     * Generate deactivate account request
     *
     * @param userVO
     * @return
     */
    public JSONObject generateDeactivateRequest(UserVO userVO)
    {
        Map<String, String> map = generateRequest(userVO);
        return generateRequest(APIConstant.DEACTIVATE_COMMAND_TAG, map);
    }

    private Map<String, String> generateRequest(UserVO userVO)
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put(APIConstant.EMAIL, userVO.getEmail());
        map.put(APIConstant.IMEI, userVO.getImei());
        map.put(APIConstant.FIRST_NAME, userVO.getFirstName());
        map.put(APIConstant.LAST_NAME, userVO.getLastName());
        map.put(APIConstant.ADDRESS, userVO.getAddress());
        map.put(APIConstant.PHONE_NUMBER, userVO.getPhoneNumber());
        map.put(APIConstant.DOB, userVO.getDob());
        map.put(APIConstant.GENDER, userVO.getGender());
        map.put(APIConstant.DEVICE, APIConstant.DEVICE_NAME);
        map.put(APIConstant.ITS_ID, userVO.getItsId());
        return map;
    }
}
