package com.burhan.soft.lisanuddawat.communication.data;

/**
 * Created by TAYEBT on 5/2/2015.
 */
public class RequestResponseVO extends BaseVO {

    private int status;
    private String command;
    private String errorMessage;
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
