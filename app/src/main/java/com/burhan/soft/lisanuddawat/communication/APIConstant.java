package com.burhan.soft.lisanuddawat.communication;

/**
 * Created by TAYEBT on 5/2/2015.
 */
public interface APIConstant {

    String SERVER_URL = "http://115.124.120.8/Lisaan-ud-Dawat/index.php";
    String PAYMENT_URL = "http://lisaanuddawat.com";

    String VERIFICATION_COMMAND_TAG = "1";
    String DEACTIVATE_COMMAND_TAG = "2";
    String PAYPAL_RESPONSE_TAG = "3";

    // Verification JSON key parameters
    String TAG = "tag";
    String EMAIL = "email";
    String IMEI = "imei";
    String PAYMENT_STATUS = "status";
    String FIRST_NAME = "fname";
    String LAST_NAME = "lname";
    String PHONE_NUMBER = "ph";
    String DOB = "dob";
    String GENDER = "gender";
    String ADDRESS = "add";
    String AMOUNT = "amt";
    String TRANSACTION_ID = "transid";
    String TRANSACTION_DATE = "transdate";
    // VISA, MASTER, AMERICAN EXPRESS, ETC
    String BANK_CODE = "bcode";
    String CARD_NUMBER = "cno";
    // CC - CREDIT CARD, DC - DEBIT CARD, NB - NET BANKING
    String MODE = "mode";
    String DEVICE = "device";
    String EXPIRY = "expiry";
    String ITS_ID = "itsid";

    String RESPONSE_CODE = "resp";
    String ERROR_MESSAGE = "errmsg";

    // Response code
    int SUCCESS = 0;
    int ERROR = 1;
    int DEACTIVATED_USER = 2;
    int USER_ALREADY_REGISTER_OTHER_DEVICE = 3;
    int NOT_REGISTERED = 4;
    int EXPIRED = 5;


    String INVOICE_DATA_KEY = "invoice";

    String IS_NOT_REGISTERED = "not_register";
    String DEACTIVATE_USER_BY_OTHER = "deactivate_user";
    String DEACTIVATED_USER_EMAIL = "deactivate_user_email";

    String DEVICE_NAME = "Android";

    String DATE_FORMAT = "dd-MMM-yyyy";

    int APP_EXPIRED = -2;
    int APP_NOT_EXPIRED = -1;
}
