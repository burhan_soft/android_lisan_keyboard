package com.burhan.soft.lisanuddawat.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.util.Util;
import com.dd.processbutton.ProcessButton;

import java.util.Random;

/**
 * Created by sandip.mahajan on 4/7/2015.
 */
public abstract class AbstractUIActivity extends ActionBarActivity {

    private int mProgress;
    private boolean isComplete = false;
    private ProcessButton button;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
    }

    /**
     * Start button processing
     *
     * @param button
     * @param handler
     */
    public void start(final ProcessButton button, final Handler handler) {
        this.button = button;
        this.handler = handler;
        isComplete = false;
        handler.postDelayed(runnable, generateDelay());
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!isComplete) {
                mProgress += 10;
                button.setProgress(mProgress);
                if (mProgress >= 90) {
                    mProgress = 0;
                }
                handler.postDelayed(this, generateDelay());
            }
        }
    };

    private Random random = new Random();

    private int generateDelay() {
        return random.nextInt(1000);
    }

    /**
     * Stop button processing
     */
    public void stop() {
        handler.removeCallbacks(runnable);
        isComplete = true;
        button.setProgress(100);
    }

    public void setErrorStop()
    {
        isComplete = true;
        button.setProgress(-1);
    }

    /**
     * Show status code
     *
     * @param errorMessage
     */
    public void showErrorMessage(String errorMessage) {
        Util.showMessage(this, getString(R.string.error), errorMessage);
        button.setProgress(-1);
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    protected abstract int getLayoutResource();

    /**
     * Handle messages received from the handler
     */
    public abstract void handleMessage(Message message);
}
