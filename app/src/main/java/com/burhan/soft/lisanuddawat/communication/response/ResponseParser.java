package com.burhan.soft.lisanuddawat.communication.response;

import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.data.RequestResponseVO;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TAYEBT on 5/2/2015.
 */
public class ResponseParser
{
    private static ResponseParser instance = new ResponseParser();

    private ResponseParser()
    {
    }

    public static ResponseParser getInstance()
    {
        return instance;
    }

    /**
     * @param response
     *
     * @return
     *
     * @throws JSONException
     */
    public synchronized RequestResponseVO parse(String response) throws JSONException
    {
        JSONObject jsonObject = new JSONObject(response);
        String tag = jsonObject.getString(APIConstant.TAG);
        if (APIConstant.VERIFICATION_COMMAND_TAG.equalsIgnoreCase(tag))
        {
            return parseVerificationResponse(jsonObject);
        }
        else if (APIConstant.DEACTIVATE_COMMAND_TAG.equalsIgnoreCase(tag))
        {
            return parseDeactivationResponse(jsonObject);
        }
        return null;
    }

    /**
     * @param jsonObject
     *
     * @return
     *
     * @throws JSONException
     */
    private RequestResponseVO parseVerificationResponse(JSONObject jsonObject) throws JSONException
    {
        RequestResponseVO requestResponseVO = new RequestResponseVO();
        int responseCode = jsonObject.getInt(APIConstant.RESPONSE_CODE);
        requestResponseVO.setStatus(responseCode);
        requestResponseVO.setCommand(APIConstant.VERIFICATION_COMMAND_TAG);
        if (responseCode == APIConstant.NOT_REGISTERED || responseCode == APIConstant.ERROR)
        {
            requestResponseVO.setData(jsonObject.getString(APIConstant.ERROR_MESSAGE));
        }
        else
        {
            UserVO userVO = getUserFromResponse(jsonObject);
            requestResponseVO.setData(userVO);
        }
        return requestResponseVO;
    }

    /**
     * @param jsonObject
     *
     * @return
     *
     * @throws JSONException
     */
    private RequestResponseVO parseDeactivationResponse(JSONObject jsonObject) throws JSONException
    {
        RequestResponseVO requestResponseVO = new RequestResponseVO();
        int responseCode = jsonObject.getInt(APIConstant.RESPONSE_CODE);
        requestResponseVO.setStatus(responseCode);
        requestResponseVO.setCommand(APIConstant.DEACTIVATE_COMMAND_TAG);
        if (responseCode != APIConstant.SUCCESS)
        {
            requestResponseVO.setData(jsonObject.getString(APIConstant.ERROR_MESSAGE));
        }
        else
        {
            UserVO userVO = getUserFromResponse(jsonObject);
            requestResponseVO.setData(userVO);
        }
        return requestResponseVO;
    }

    private UserVO getUserFromResponse(JSONObject jsonObject) throws JSONException
    {
        UserVO userVO = new UserVO();
        userVO.setEmail(jsonObject.getString(APIConstant.EMAIL));
        userVO.setImei(jsonObject.getString(APIConstant.IMEI));
        userVO.setFirstName(jsonObject.getString(APIConstant.FIRST_NAME));
        userVO.setLastName(jsonObject.getString(APIConstant.LAST_NAME));
        userVO.setPhoneNumber(jsonObject.getString(APIConstant.PHONE_NUMBER));
        userVO.setDob(jsonObject.getString(APIConstant.DOB));
        userVO.setAddress(jsonObject.getString(APIConstant.ADDRESS));

        userVO.setTransactionId(jsonObject.getString(APIConstant.TRANSACTION_ID));
        userVO.setTransactionDate(jsonObject.getString(APIConstant.TRANSACTION_DATE));
        userVO.setStatus(jsonObject.getString(APIConstant.PAYMENT_STATUS));
        userVO.setAmount(jsonObject.getString(APIConstant.AMOUNT));
        userVO.setBankCode(jsonObject.getString(APIConstant.BANK_CODE));
        userVO.setCardNumber(jsonObject.getString(APIConstant.CARD_NUMBER));
        userVO.setMode(jsonObject.getString(APIConstant.MODE));
        userVO.setItsId(jsonObject.getString(APIConstant.ITS_ID));
        userVO.setExpiry(jsonObject.getInt(APIConstant.EXPIRY));
        return userVO;
    }
}
