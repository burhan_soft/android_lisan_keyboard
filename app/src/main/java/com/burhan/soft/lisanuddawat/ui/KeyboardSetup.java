package com.burhan.soft.lisanuddawat.ui;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.burhan.soft.lisanuddawat.R;
import com.burhan.soft.lisanuddawat.communication.Cache;
import com.burhan.soft.lisanuddawat.service.VerificationService;
import com.burhan.soft.lisanuddawat.util.Util;
import com.burhan.soft.lisanuddawat.widget.LisanWidget;


public class KeyboardSetup extends ActionBarActivity {

    private static final int ENABLE_KEYBOARD = 1;
    private static final int SWITCH_KEYBOARD = 2;
    private LinearLayout enable_btn;
    private LinearLayout switch_btn;
    private LinearLayout test_btn;
    private TextView enableKeyboardDesc;
    private TextView switchKeyboardDesc;
    private static final int KEY_MESSAGE_UNREGISTER_LISTENER = 447;
    private static final int KEY_MESSAGE_RETURN_TO_APP = 446;
    private Context mAppContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard_setup);
        enable_btn = (LinearLayout) findViewById(R.id.enable_keyboard);
        switch_btn = (LinearLayout) findViewById(R.id.switch_keyboard);
        test_btn = (LinearLayout) findViewById(R.id.test_keyboard);
        enableKeyboardDesc = (TextView) findViewById(R.id.enable_keyboard_desc);
        switchKeyboardDesc = (TextView) findViewById(R.id.switch_keyboard_desc);
        //registering for changes, so I'll know to come back here.
        mAppContext = getApplicationContext();
        mAppContext.getContentResolver().registerContentObserver(Settings.Secure.CONTENT_URI, true, mSecureSettingsChanged);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Startup.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.input_method_fancy_enter, R.anim.input_method_fancy_exit);
    }

    private Handler mGetBackHereHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case KEY_MESSAGE_RETURN_TO_APP:
                    onStart();
                    break;
                case KEY_MESSAGE_UNREGISTER_LISTENER:
                    unregisterSettingsObserverNow();
                    break;
            }
        }
    };

    private final ContentObserver mSecureSettingsChanged = new ContentObserver(null) {
        @Override
        public boolean deliverSelfNotifications() {
            return false;
        }

        @Override
        public void onChange(boolean selfChange) {
            if (isStepCompleted()) {
                //should we return to this task?
                //this happens when the user is asked to enable AnySoftKeyboard, which is done on a different UI activity (outside of my App).
                mGetBackHereHandler.removeMessages(KEY_MESSAGE_RETURN_TO_APP);
                mGetBackHereHandler.sendMessageDelayed(mGetBackHereHandler.obtainMessage(KEY_MESSAGE_RETURN_TO_APP), 50/*enough for the user to see what happened.*/);
                Intent intent = new Intent();
                intent.setAction(LisanWidget.WIDGET_TEXT_CHANGE_ACTION);
                sendBroadcast(intent);
            }
        }
    };

    protected boolean isStepCompleted() {
        return Util.isThisKeyboardEnabled(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterSettingsObserverNow();
    }

    private void unregisterSettingsObserverNow() {
        mGetBackHereHandler.removeMessages(KEY_MESSAGE_UNREGISTER_LISTENER);
        if (mAppContext != null) {
            mAppContext.getContentResolver().unregisterContentObserver(mSecureSettingsChanged);
            mAppContext = null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        switch_btn.setVisibility(View.GONE);
        test_btn.setVisibility(View.GONE);
        if (Util.isThisKeyboardEnabled(this)) {
            enableKeyboardDesc.setText(getString(R.string.enabled_keyboard_description));
            enableKeyboardDesc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sym_keyboard_done, 0, 0, 0);
            switch_btn.setVisibility(View.VISIBLE);
            if (Util.isThisKeyboardSetAsDefaultIME(this)) {
                switchKeyboardDesc.setText(getString(R.string.switched_keyboard_description));
                switchKeyboardDesc.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sym_keyboard_done, 0, 0, 0);
                test_btn.setVisibility(View.VISIBLE);

            } else {
                switchKeyboardDesc.setText(getString(R.string.switch_keyboard_description));
                switchKeyboardDesc.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        } else {
            enableKeyboardDesc.setText(getString(R.string.enable_keyboard_description));
            enableKeyboardDesc.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void enableKeyboard(View view) {
        Intent enableIntent = new Intent(android.provider.Settings.ACTION_INPUT_METHOD_SETTINGS);
        startActivityForResult(enableIntent, ENABLE_KEYBOARD);
    }

    public void switchKeyboard(View view) {
        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        if (imeManager != null) {
            imeManager.showInputMethodPicker();
        } else {
            Toast.makeText(this, "not possible in picker", Toast.LENGTH_LONG).show();
        }
    }

    public void testKeyboard(View view) {
        Intent testIntent = new Intent(this, Launch.class);
        startActivity(testIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ENABLE_KEYBOARD) {
                enable_btn.setEnabled(false);
                switch_btn.setEnabled(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsPreference.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
