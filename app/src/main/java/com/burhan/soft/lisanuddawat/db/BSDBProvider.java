package com.burhan.soft.lisanuddawat.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import java.sql.SQLException;

/**
 * Created by sandip.mahajan on 4/5/2015.
 */
public class BSDBProvider extends ContentProvider {

    private static final UriMatcher sURIMatcher = buildUriMatcher();
    public static String AUTHORITY = "com.burhan.soft.lisanuddawat.db.BSDBProvider";
    public static Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/dictionary");
    public static String WORDS_MIME_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + ".dictionary";
    String TAG = "BSDBProvider";
    /**
     * Database
     */
    private BSDatabase mDictionary;

    /**
     * Build URI matcher for the content provider
     *
     * @return
     */
    private static UriMatcher buildUriMatcher() {
        AUTHORITY = "com.burhan.soft.lisanuddawat.db.BSDBProvider";
        CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/dictionary");
        WORDS_MIME_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + ".dictionary";
        UriMatcher localUriMatcher = new UriMatcher(-1);
        localUriMatcher.addURI(AUTHORITY, "dictionary", 0);
        localUriMatcher.addURI(AUTHORITY, "dictionary/#", 1);
        return localUriMatcher;
    }

    /**
     * Implement this to initialize your content provider on startup.
     * This method is called for all registered content providers on the
     * application main thread at application launch time.  It must not perform
     * lengthy operations, or application startup will be delayed.
     * <p/>
     * <p>You should defer nontrivial initialization (such as opening,
     * upgrading, and scanning databases) until the content provider is used
     * (via {@link #query}, {@link #insert}, etc).  Deferred initialization
     * keeps application startup fast, avoids unnecessary work if the provider
     * turns out not to be needed, and stops database errors (such as a full
     * disk) from halting application launch.
     * <p/>
     * <p>If you use SQLite, {@link android.database.sqlite.SQLiteOpenHelper}
     * is a helpful utility class that makes it easy to manage databases,
     * and will automatically defer opening until first use.  If you do use
     * SQLiteOpenHelper, make sure to avoid calling
     * {@link android.database.sqlite.SQLiteOpenHelper#getReadableDatabase} or
     * {@link android.database.sqlite.SQLiteOpenHelper#getWritableDatabase}
     * from this method.  (Instead, override
     * {@link android.database.sqlite.SQLiteOpenHelper#onOpen} to initialize the
     * database when it is first opened.)
     *
     * @return true if the provider was successfully loaded, false otherwise
     */
    @Override
    public boolean onCreate() {
        this.mDictionary = new BSDatabase(getContext());
        try {
            mDictionary.createDatabase();
            mDictionary.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Implement this to handle query requests from clients.
     * This method can be called from multiple threads, as described in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html#Threads">Processes
     * and Threads</a>.
     * <p/>
     * Example client call:<p>
     * <pre>// Request a specific record.
     * Cursor managedCursor = managedQuery(
     * ContentUris.withAppendedId(Contacts.People.CONTENT_URI, 2),
     * projection,    // Which columns to return.
     * null,          // WHERE clause.
     * null,          // WHERE clause value substitution
     * People.NAME + " ASC");   // Sort order.</pre>
     * Example implementation:<p>
     * <pre>// SQLiteQueryBuilder is a helper class that creates the
     * // proper SQL syntax for us.
     * SQLiteQueryBuilder qBuilder = new SQLiteQueryBuilder();
     * <p/>
     * // Set the table we're querying.
     * qBuilder.setTables(DATABASE_TABLE_NAME);
     * <p/>
     * // If the query ends in a specific record number, we're
     * // being asked for a specific record, so set the
     * // WHERE clause in our query.
     * if((URI_MATCHER.match(uri)) == SPECIFIC_MESSAGE){
     * qBuilder.appendWhere("_id=" + uri.getPathLeafId());
     * }
     * <p/>
     * // Make the query.
     * Cursor c = qBuilder.query(mDb,
     * projection,
     * selection,
     * selectionArgs,
     * groupBy,
     * having,
     * sortOrder);
     * c.setNotificationUri(getContext().getContentResolver(), uri);
     * return c;</pre>
     *
     * @param uri           The URI to query. This will be the full URI sent by the client;
     *                      if the client is requesting a specific record, the URI will end in a record number
     *                      that the implementation should parse and add to a WHERE or HAVING clause, specifying
     *                      that _id value.
     * @param projection    The list of columns to put into the cursor. If
     *                      {@code null} all columns are included.
     * @param selection     A selection criteria to apply when filtering rows.
     *                      If {@code null} then all rows are included.
     * @param selectionArgs You may include ?s in selection, which will be replaced by
     *                      the values from selectionArgs, in order that they appear in the selection.
     *                      The values will be bound as Strings.
     * @param sortOrder     How the rows in the cursor should be sorted.
     *                      If {@code null} then the provider is free to define the sort order.
     * @return a Cursor or {@code null}.
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (DBConstant.SEARCH_USER_RECORD.equalsIgnoreCase((sortOrder))) {
            return searchUserRecord(selection);
        } else if (DBConstant.SEARCH_PREDICTION.equalsIgnoreCase((sortOrder))) {
            return searchPrediction(selection);
        }
        return search(selection);
    }

    /**
     * Implement this to handle requests for the MIME type of the data at the
     * given URI.  The returned MIME type should start with
     * <code>vnd.android.cursor.item</code> for a single record,
     * or <code>vnd.android.cursor.dir/</code> for multiple items.
     * This method can be called from multiple threads, as described in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html#Threads">Processes
     * and Threads</a>.
     * <p/>
     * <p>Note that there are no permissions needed for an application to
     * access this information; if your content provider requires read and/or
     * write permissions, or is not exported, all applications can still call
     * this method regardless of their access permissions.  This allows them
     * to retrieve the MIME type for a URI when dispatching intents.
     *
     * @param paramUri the URI to query.
     * @return a MIME type string, or {@code null} if there is no type.
     */
    @Override
    public String getType(Uri paramUri) {
        return WORDS_MIME_TYPE;
    }

    /**
     * Implement this to handle requests to insert a new row.
     * As a courtesy, call {@link android.content.ContentResolver#notifyChange(android.net.Uri, android.database.ContentObserver) notifyChange()}
     * after inserting.
     * This method can be called from multiple threads, as described in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html#Threads">Processes
     * and Threads</a>.
     *
     * @param uri    The content:// URI of the insertion request. This must not be {@code null}.
     * @param values A set of column_name/value pairs to add to the database.
     *               This must not be {@code null}.
     * @return The URI for the newly inserted item.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID = mDictionary.insertUserData(values);
        Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
        return _uri;
    }

    /**
     * Implement this to handle requests to delete one or more rows.
     * The implementation should apply the selection clause when performing
     * deletion, allowing the operation to affect multiple rows in a directory.
     * As a courtesy, call {@link android.content.ContentResolver#notifyChange(android.net.Uri, android.database.ContentObserver) notifyChange()}
     * after deleting.
     * This method can be called from multiple threads, as described in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html#Threads">Processes
     * and Threads</a>.
     * <p/>
     * <p>The implementation is responsible for parsing out a row ID at the end
     * of the URI, if a specific row is being deleted. That is, the client would
     * pass in <code>content://contacts/people/22</code> and the implementation is
     * responsible for parsing the record number (22) when creating a SQL statement.
     *
     * @param uri           The full URI to query, including a row ID (if a specific record is requested).
     * @param selection     An optional restriction to apply to rows when deleting.
     * @param selectionArgs
     * @return The number of rows affected.
     * @throws java.sql.SQLException
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return mDictionary.deleteUserData(selection);
    }

    /**
     * Implement this to handle requests to update one or more rows.
     * The implementation should update all rows matching the selection
     * to set the columns according to the provided values map.
     * As a courtesy, call {@link android.content.ContentResolver#notifyChange(android.net.Uri, android.database.ContentObserver) notifyChange()}
     * after updating.
     * This method can be called from multiple threads, as described in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html#Threads">Processes
     * and Threads</a>.
     *
     * @param uri           The URI to query. This can potentially have a record ID if this
     *                      is an update request for a specific record.
     * @param values        A set of column_name/value pairs to update in the database.
     *                      This must not be {@code null}.
     * @param selection     An optional filter to match rows to update.
     * @param selectionArgs
     * @return the number of rows affected.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    /**
     * Search words from database
     *
     * @param paramString
     * @return
     */
    private Cursor search(String paramString) {
        String str = paramString.toLowerCase();
        String[] arrayOfString = {BSDatabase.KEY_ID, BSDatabase.KEY_WORD, BSDatabase.KEY_WORD_ENGLISH};
        return this.mDictionary.getWordMatches(str, arrayOfString);
    }

    /**
     * Search user record from database
     *
     * @param paramString
     * @return
     */
    private Cursor searchUserRecord(String paramString) {
        String str = paramString.toLowerCase();
        return this.mDictionary.getUserRecord(str, null);
    }

    /**
     * Search words from database
     *
     * @param paramString
     * @return
     */
    private Cursor searchPrediction(String paramString) {
        String str = paramString.toLowerCase();
        String[] arrayOfString = {BSDatabase.PREDICTION_KEY_ID, BSDatabase.PREDICTION_KEY_WORD, BSDatabase.PREDICTION_KEY_WORD_DESCRIPTION};
        return this.mDictionary.getWordEquals(str, arrayOfString);
    }
}
