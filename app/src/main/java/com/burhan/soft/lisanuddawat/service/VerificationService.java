package com.burhan.soft.lisanuddawat.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;

import com.burhan.soft.lisanuddawat.communication.Cache;
import com.burhan.soft.lisanuddawat.communication.business.CommunicationHandlerManager;
import com.burhan.soft.lisanuddawat.communication.data.RequestResponseVO;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;
import com.burhan.soft.lisanuddawat.ui.LisanApplication;
import com.burhan.soft.lisanuddawat.util.Util;

public class VerificationService extends Service implements BaseService {

    private static final int VERIFY_USER_SERVICE_TIME = 1 * 60 * 60 * 1000;
    public static final String RESULT = "result";
    public static final String NOTIFICATION = "com.burhan.soft.lisanuddawat.service";

    private Util.ServiceHandler serviceHandler = new Util.ServiceHandler(this);

    /**
     * Called by the system every time a client explicitly starts the service by calling
     * {@link android.content.Context#startService}, providing the arguments it supplied and a
     * unique integer token representing the start request.  Do not call this method directly.
     * <p/>
     * <p>For backwards compatibility, the default implementation calls
     * {@link #onStart} and returns either {@link #START_STICKY}
     * or {@link #START_STICKY_COMPATIBILITY}.
     * <p/>
     * <p>If you need your application to run on platform versions prior to API
     * level 5, you can use the following model to handle the older {@link #onStart}
     * callback in that case.  The <code>handleCommand</code> method is implemented by
     * you as appropriate:
     * <p/>
     * {@sample development/samples/ApiDemos/src/com/example/android/apis/app/ForegroundService.java
     * start_compatibility}
     * <p/>
     * <p class="caution">Note that the system calls this on your
     * service's main thread.  A service's main thread is the same
     * thread where UI operations take place for Activities running in the
     * same process.  You should always avoid stalling the main
     * thread's event loop.  When doing long-running operations,
     * network calls, or heavy disk I/O, you should kick off a new
     * thread, or use {@link android.os.AsyncTask}.</p>
     *
     * @param intent  The Intent supplied to {@link android.content.Context#startService},
     *                as given.  This may be null if the service is being restarted after
     *                its process has gone away, and it had previously returned anything
     *                except {@link #START_STICKY_COMPATIBILITY}.
     * @param flags   Additional data about this start request.  Currently either
     *                0, {@link #START_FLAG_REDELIVERY}, or {@link #START_FLAG_RETRY}.
     * @param startId A unique integer representing this specific request to
     *                start.  Use with {@link #stopSelfResult(int)}.
     * @return The return value indicates what semantics the system should
     * use for the service's current started state.  It may be one of the
     * constants associated with the {@link #START_CONTINUATION_MASK} bits.
     * @see #stopSelfResult(int)
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LisanApplication.setServiceHandler(serviceHandler);
        serviceHandler.postDelayed(runnable, 0);
        return Service.START_NOT_STICKY;
    }

    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * {@link android.os.IBinder} is usually for a complex interface
     * that has been <a href="{@docRoot}guide/components/aidl.html">described using
     * aidl</a>.
     * <p/>
     * <p><em>Note that unlike other application components, calls on to the
     * IBinder interface returned here may not happen on the main thread
     * of the process</em>.  More information about the main thread can be found in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html">Processes and
     * Threads</a>.</p>
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link android.content.Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // verify user if it is valid or not
            UserVO userVO = Cache.getInstance().getUserVO();
            if (userVO == null) {
                userVO = Util.getRegisterUserData(getApplicationContext());
            }
            if (userVO != null) {
                Cache.getInstance().setServiceCalled(true);
                CommunicationHandlerManager.getInstance().verifyUser(userVO);
            }
            serviceHandler.postDelayed(runnable, VERIFY_USER_SERVICE_TIME);
        }
    };

    @Override
    public void handleMessage(Message msg) {
        Object o = msg.obj;
        if (o instanceof RequestResponseVO) {
            RequestResponseVO user = (RequestResponseVO) o;
            publishResults(user);
        }
    }

    private void publishResults(RequestResponseVO o) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, o);
        sendBroadcast(intent);
    }
}
