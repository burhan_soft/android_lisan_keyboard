package com.burhan.soft.lisanuddawat.ui;

import android.content.Context;
import android.graphics.Color;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class CheckBoxPreferenceClass extends CheckBoxPreference {


    public CheckBoxPreferenceClass(Context context) {
        super(context);
    }

    public CheckBoxPreferenceClass(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckBoxPreferenceClass(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        TextView title = (TextView) view.findViewById(android.R.id.title);
        TextView description = (TextView) view.findViewById(android.R.id.summary);
        title.setTextColor(Color.BLACK);
        description.setTextColor(Color.GRAY);
    }

}