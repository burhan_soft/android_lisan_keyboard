package com.burhan.soft.lisanuddawat.communication.business;

import com.burhan.soft.lisanuddawat.communication.APIConstant;
import com.burhan.soft.lisanuddawat.communication.RestWrapperAsync;
import com.burhan.soft.lisanuddawat.communication.data.RequestResponseVO;
import com.burhan.soft.lisanuddawat.communication.data.UserVO;
import com.burhan.soft.lisanuddawat.communication.request.RequestGenerator;
import com.burhan.soft.lisanuddawat.communication.response.ResponseParser;
import com.burhan.soft.lisanuddawat.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TAYEBT on 5/4/2015.
 */
public class CommunicationHandlerManager {

    private static CommunicationHandlerManager instance = new CommunicationHandlerManager();

    private CommunicationHandlerManager() {
    }

    public static CommunicationHandlerManager getInstance() {
        return instance;
    }

    /**
     * Call back to handle response from the server
     */
    RestWrapperAsync.ResponseCallback responseCallback = new RestWrapperAsync.ResponseCallback() {
        @Override
        public void OnRequestSuccess(String response) {
            try {
                RequestResponseVO requestResponseVO = ResponseParser.getInstance().parse(response);
                Util.sendResponseMessage(requestResponseVO.getCommand(), requestResponseVO);
            } catch (JSONException e) {
                e.printStackTrace();
                Util.sendResponseMessage(APIConstant.ERROR + "", e);
            }
        }

        @Override
        public void OnRequestError(Exception ex) {
            Util.sendResponseMessage(APIConstant.ERROR + "", ex);
        }
    };

    /**
     * Verify user with the server, if the user is available or not.
     *
     * @param userVO
     */
    public void verifyUser(UserVO userVO) {
        RestWrapperAsync restWrapperAsync = new RestWrapperAsync();
        restWrapperAsync.setResponseCallback(responseCallback);
        JSONObject jsonObject = RequestGenerator.getInstance().generateVerifyRequest(userVO);
        restWrapperAsync.setFormBodyJson(jsonObject);
        restWrapperAsync.execute((Void) null);
    }

    /**
     * Deactivate current user
     *
     * @param userVO
     */
    public void deactivateUser(UserVO userVO) {
        RestWrapperAsync restWrapperAsync = new RestWrapperAsync();
        restWrapperAsync.setResponseCallback(responseCallback);
        JSONObject jsonObject = RequestGenerator.getInstance().generateDeactivateRequest(userVO);
        restWrapperAsync.setFormBodyJson(jsonObject);
        restWrapperAsync.execute((Void) null);
    }
}
