package com.burhan.soft.lisanuddawat.ui;

import android.os.Bundle;
import android.os.Message;
import android.widget.Button;

import com.burhan.soft.lisanuddawat.R;

/**
 * Launch activity
 */
public class Launch extends AbstractUIActivity {

    /**
     * Show options to enable keyboard
     */
    private Button btnEnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_launch;
    }

    /**
     * Handle messages received from the handler
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }
}
