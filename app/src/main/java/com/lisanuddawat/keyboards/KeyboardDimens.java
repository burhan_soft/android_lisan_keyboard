package com.lisanuddawat.keyboards;

public interface KeyboardDimens {

	int getKeyboardMaxWidth();
	
	float getKeyHorizontalGap();
	float getRowVerticalGap();
	
	int getNormalKeyHeight();
	int getSmallKeyHeight();
	int getLargeKeyHeight();
}
