package com.lisanuddawat.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.burhan.soft.lisanuddawat.ui.LisanApplication;
import com.lisanuddawat.LisanSoftKeyboard;
import com.lisanuddawat.addons.AddOnsFactory;

public class PackagesChangedReceiver extends BroadcastReceiver {
	
	private static final String TAG = "ASK PkgChanged";
	
	private final LisanSoftKeyboard mIme;
	private final StringBuffer mSB = new StringBuffer();
	
	public PackagesChangedReceiver(LisanSoftKeyboard ime)
	{
		mIme = ime;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent == null || intent.getData() == null || context == null)
			return;

		if (LisanApplication.DEBUG)
		{
			mSB.setLength(0);
			String text = mSB.append("Package '").append(intent.getData()).append("' have been changed.").toString();
			Log.d(TAG, text);
		}
		AddOnsFactory.onPackageChanged(intent, mIme);
	}

	public IntentFilter createFilterToRegisterOn() {
		/*
		receiver android:name="com.lisanuddawat.receivers.PackagesChangedReceiver">
	    	<intent-filter>
	    		<category android:name="android.intent.category.DEFAULT" />
	    		<action android:name="android.intent.action.PACKAGE_CHANGED"/>
	    		<action android:name="android.intent.action.PACKAGE_REMOVED"/>
	    		<action android:name="android.intent.action.PACKAGE_ADDED"/>
	    		<action android:name="android.intent.action.PACKAGE_INSTALL"/>
	    		<action android:name="android.intent.action.PACKAGE_REPLACED"/>
	    		<data android:scheme="package" />
	    	</intent-filter>	    	
	    </receiver>
		 */
		IntentFilter filter = new IntentFilter();
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		
		filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
		filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
		filter.addAction(Intent.ACTION_PACKAGE_ADDED);
		filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
		
		filter.addDataScheme("package");
		
		return filter;
	}
}
