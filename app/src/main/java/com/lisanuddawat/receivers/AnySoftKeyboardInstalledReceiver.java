package com.lisanuddawat.receivers;

import com.burhan.soft.lisanuddawat.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/*
 * This receiver is (hopefully) called after AnySoftKeyboard installed from the Market
 * see:
 * http://code.google.com/mobile/analytics/docs/android/#referrals
 * http://stackoverflow.com/questions/4093150/get-referrer-after-installing-app-from-android-market
 * 
 * In Market v3+ this will not happen anymore :(
 */
public class AnySoftKeyboardInstalledReceiver extends BroadcastReceiver {

	private static final String TAG = "ASK Installed";
	public  static final int INSTALLED_NOTIFICATION_ID = 45711;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		Log.i(TAG, "Thank you for installing AnySoftKeyboard! We hope you'll like it.");
	}
}