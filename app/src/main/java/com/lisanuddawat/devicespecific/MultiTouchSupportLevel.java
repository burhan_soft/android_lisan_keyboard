package com.lisanuddawat.devicespecific;

public enum MultiTouchSupportLevel {
	None,
	Basic,
	Distinct
}
