package com.lisanuddawat.devicespecific;

import com.lisanuddawat.dictionaries.DictionaryFactory;
import com.lisanuddawat.voice.VoiceInput;

import android.content.Context;
import android.inputmethodservice.InputMethodService;
import android.view.GestureDetector;
import android.view.MotionEvent;

public interface DeviceSpecific {

	public abstract String getApiLevel();
	
	public WMotionEvent createMotionEventWrapper(MotionEvent nativeMotionEvent);
	
	public MultiTouchSupportLevel getMultiTouchSupportLevel(Context appContext);
	
	public GestureDetector createGestureDetector(Context appContext, AskOnGestureListener listener);

	public DictionaryFactory createDictionaryFactory();

	public VoiceInput createVoiceInput(InputMethodService ime);
	
	public Clipboard getClipboard(Context appContext);
}
