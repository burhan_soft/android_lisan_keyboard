package com.lisanuddawat.devicespecific;

public interface Clipboard {
	CharSequence getText();
	void setText(CharSequence text);
}
