package com.lisanuddawat;


import android.content.Context;
import android.content.SharedPreferences;

public interface LisanKeyboardContextProvider
{
	Context getApplicationContext();
	void deleteLastCharactersFromInput(int lenght);
	//void appendCharactersToInput(CharSequence text);
	SharedPreferences getSharedPreferences();
	//void showToastMessage(int resId, boolean forShortTime);
	//void performLengthyOperation(int textResId, final Runnable thingToDo);
}
