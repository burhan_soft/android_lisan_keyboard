package com.lisanuddawat.dictionaries;

import android.content.Context;
import android.util.Log;

import com.burhan.soft.lisanuddawat.ui.LisanApplication;
import com.lisanuddawat.LisanSoftKeyboard;

public class DictionaryFactory
{
    private static final String TAG = "ASK DictFctry";
    
    private static final DictionaryFactory msFactory;
    
    static
    {
    	msFactory = LisanApplication.getDeviceSpecific().createDictionaryFactory();
    }
    
    public static DictionaryFactory getInstance()
    {
    	return msFactory;
    }
    
    private AutoDictionary mAutoDictionary = null;
    private String mUserDictionaryLocale = null;
    private EditableDictionary mUserDictionary = null;

    public DictionaryFactory()
    {
    }
    
    public synchronized EditableDictionary createUserDictionary(Context context, String locale)
    {
        if (mUserDictionary != null && equalsString(mUserDictionaryLocale, locale)){
            return mUserDictionary;
        }
        
        mUserDictionary = new SafeUserDictionary(context, locale);
        mUserDictionary.loadDictionary();
        
        mUserDictionaryLocale = locale;
        return mUserDictionary;
    }
    
    public synchronized EditableDictionary createContactsDictionary(Context context)
    {
          return null;
    }
    
    public boolean equalsString(String a, String b){
        if(a == null && b == null){
            return true;
        }
        if(a == null || b == null){
            return false;
        }
        return a.equals(b);
    }
    
    
    public synchronized AutoDictionary createAutoDictionary(Context context, LisanSoftKeyboard ime, String currentAutoDictionaryLocale)
    {
    	if (!LisanApplication.getConfig().useAutoDictionary())
    		return null;
    	
    	if (mAutoDictionary != null && equalsString(mAutoDictionary.getLocale(), currentAutoDictionaryLocale))
    	{
    		return mAutoDictionary;
    	}
    	
    	Log.d(TAG, "Creating AutoDictionary for locale: "+currentAutoDictionaryLocale);
        mAutoDictionary = new AutoDictionary(context, ime, currentAutoDictionaryLocale);
        mAutoDictionary.loadDictionary();
        
        return mAutoDictionary;
    }
}
